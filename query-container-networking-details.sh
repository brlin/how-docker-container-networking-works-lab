#!/usr/bin/env bash
# Query container networking details from the Docker daemon
#
# Copyright 2023 林博仁(Buo-ren, Lin) <buo.ren.lin@gmail.com>
# SPDX-License-Identifier: CC-BY-SA-4.0+
CONTAINER="${CONTAINER:-test}"

set_opts=(
    -o errexit
    -o errtrace
    -o nounset
)
if ! set "${set_opts[@]}"; then
    printf \
        'Error: Unable to set the defensive script interpreter behavior.\n' \
        1>&2
    exit 1
fi

filter='Name={{.Name}}'
filter+=' Hostname={{.Config.Hostname}}'
filter+=' IP={{or .NetworkSettings.IPAddress .NetworkSettings.Networks.testnet.IPAddress}}'
filter+=' Mac={{or .NetworkSettings.MacAddress .NetworkSettings.Networks.testnet.MacAddress}}'
# FIXME: What is "testnet"?
filter+=' Bridge={{if .NetworkSettings.IPAddress}}docker0{{else}}testnet{{end}}'

if ! docker_inspect_raw="$(
    docker inspect "${CONTAINER}" \
        --format "${filter}"
    )"; then
    printf \
        'Error: Unable to inspect the "%s" Docker container.\n' \
        "${CONTAINER}" \
        1>&2
    exit 2
fi

if ! docker_inspect_result="$(
    sed 's/=\//=/g' <<<"${docker_inspect_raw}"
    )"; then
    printf \
        'Error: Unable to post-process the docker inspect results.\n' \
        1>&2
    exit 2
fi

printf \
    'Info: Docker inspect result of the "%s" container:\n%s\n' \
    "${CONTAINER}" \
    "${docker_inspect_result}"
exit 0
