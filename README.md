# "How Docker Container Networking Works - Mimic It Using Linux Network Namespaces - DEV Community" lab

## References

* [How Docker Container Networking Works - Mimic It Using Linux Network Namespaces - DEV Community](https://dev.to/polarbit/how-docker-container-networking-works-mimic-it-using-linux-network-namespaces-9mj)  
  The original article to try on
* [veth(4) - Linux manual page](https://www.man7.org/linux/man-pages/man4/veth.4.html)  
  Documentation of the veth virtual ethernet device interface
* [‎Bash query running Docker containers | Google Bard](https://bard.google.com/share/ac5a1ada2b62)  
  Explains how to query the list of running Docker conatiners in a Bash shell script
