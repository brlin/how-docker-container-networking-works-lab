#!/usr/bin/env bash
# Make Docker network namespaces queryable by the `ip netns list` command
#
# Copyright _copyright_effective_year_ _copyright_holder_name_ <_copyright_holder_contact_>
# SPDX-License-Identifier: CC-BY-SA-4.0

set_opts=(
    # Terminate script execution when an unhandled error occurs
    -o errexit
    -o errtrace

    # Terminate script execution when an unset parameter variable is
    # referenced
    -o nounset
)
if ! set "${set_opts[@]}"; then
    printf \
        'Error: Unable to set the defensive interpreter behavior.\n' \
        1>&2
    exit 1
fi

if ! shopt -s nullglob; then
    printf \
        'Error: Unable to set the nullglob interpreter behavior.\n' \
        1>&2
    exit 1
fi

required_commands=(
    docker
    realpath
)
flag_dependency_check_failed=false
for command in "${required_commands[@]}"; do
    if ! command -v "${command}" >/dev/null; then
        flag_dependency_check_failed=true
        printf \
            'Error: Unable to locate the "%s" command in the command search PATHs.\n' \
            "${command}" \
            1>&2
    fi
done
if test "${flag_dependency_check_failed}" == true; then
    printf \
        'Error: Dependency check failed, please check your installation.\n' \
        1>&2
    exit 1
fi

if test -v BASH_SOURCE; then
    # Convenience variables
    # shellcheck disable=SC2034
    {
        script="$(
            realpath \
                --strip \
                "${BASH_SOURCE[0]}"
        )"
        script_dir="${script%/*}"
        script_filename="${script##*/}"
        script_name="${script_filename%%.*}"
    }
fi

if test "${EUID}" -ne 0; then
    printf \
        'Error: This program is required to be run as the superuser(root).\n' \
        1>&2
    exit 1
fi

runtime_network_namespace_basedir=/var/run/netns
if ! test -e "${runtime_network_namespace_basedir}"; then
    printf \
        'Info: Ensuring that the runtime network namespace directory(%s) is available...\n' \
        "${runtime_network_namespace_basedir}"
    if ! mkdir -p "${runtime_network_namespace_basedir}"; then
        printf \
            'Error: Unable to create the runtime network namespace directory.\n' \
            1>&2
        exit 2
    fi
fi

# ‎Bash query running Docker containers | Google Bard
# https://bard.google.com/share/ac5a1ada2b62
docker_ps_opts=(
    # Only show container IDs
    -q
)
if ! running_docker_container_ids_raw="$(
    docker ps "${docker_ps_opts[@]}"
    )"; then
    printf \
        'Error: Unable to query the list of running Docker container identifiers.\n' \
        1>&2
    exit 2
fi

mapfile_opts=(
    # Remove trailing line endings from each input member
    -t
)
if ! \
    mapfile \
        "${mapfile_opts[@]}" \
        running_docker_container_ids \
        <<<"${running_docker_container_ids_raw}"; then
    printf \
        'Error: Unable to convert the raw list of of running Docker container identifiers into a Bash array.\n' \
        1>&2
    exit 2
fi

for container_id in "${running_docker_container_ids[@]}"; do
    if ! container_name="$(
        docker inspect -f '{{.Name}}' "${container_id}"
        )"; then
        printf \
            'Error: Unable to query the descriptive container name from the container ID "%s".\n' \
            "${container_id}" \
            1>&2
        exit 2
    fi

    # Remove prefixes from container name
    #
    # Docker inspect has extra forward slash in name · Issue #6705 · moby/moby
    # https://github.com/moby/moby/issues/6705#issuecomment-47298276
    container_name="${container_name##*/}"

    printf \
        'Info: Processing container "%s"("%s")...\n' \
        "${container_name}" \
        "${container_id}"

    printf \
        'Info: Querying the process identifier for the Docker container "%s"...\n' \
        "${container_name}"
    if ! container_pid="$(
        docker inspect "${container_name}" \
            --format '{{.State.Pid}}'
        )"; then
        printf \
            'Error: Unable to query the process ID of the "%s" Docker container.\n' \
            "${container_name}" \
            1>&2
        exit 2
    fi
    printf \
        'Info: Process ID of the "%s" Docker container determined to be "%s".\n' \
        "${container_name}" \
        "${container_pid}"

    runtime_network_namespace_dir="${runtime_network_namespace_basedir}/${container_name}"
    if test -e "${runtime_network_namespace_dir}"; then
        printf \
            'Info: The runtime network namespace directory for the Docker container "%s" is already existing, skipping...\n' \
            "${container_name}"
        continue
    fi

    procfs_network_namespace_dir="/proc/${container_pid}/ns/net"
    printf \
        'Info: Recreating the runtime network namespace directory for the Docker container "%s"...\n' \
        "${container_name}"
    ln_opts=(
        --symbolic
    )
    if ! \
        ln \
            "${ln_opts[@]}" \
            "${procfs_network_namespace_dir}" \
            "${runtime_network_namespace_dir}"; then
        printf \
            'Error: Unable to recreate the runtime network namespace directory for the Docker container "%s"...\n' \
            "${container_name}" \
            1>&2
        exit 2
    fi
done

printf \
    'Info: Operation completed without errors.\n'
exit 0
